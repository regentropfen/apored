﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ToggleSoundBehaviour : AbstractMenuItemBehaviour {
	public TextMeshPro TextMesh;
	string SoundActiveText = "<size=125%>S</size>ound<size=125%> A</size>n";
	string SoundOffText = "<size=125%>S</size>ound<size=125%> A</size>us";


	protected override void Start()
	{
		base.Start();
		Hit();
		Hit();
	}
	public override void Hit()
    {
	GameContext.Sound = !GameContext.Sound;
		if (GameContext.Sound)
		{
			TextMesh.text = SoundActiveText;
		}else{
			TextMesh.text = SoundOffText;
		}
	}
}
