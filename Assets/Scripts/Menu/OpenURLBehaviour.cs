﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenURLBehaviour : MonoBehaviour {

	public string URL = "http://www.regentropfen.org";

	public void OpenUrl()
    {
		Application.OpenURL(URL);
    }
}
