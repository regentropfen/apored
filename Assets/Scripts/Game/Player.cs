﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using APO;

namespace APO
{
    public class Player : MonoBehaviour
    {
        Animator _animator;
        GameController instance;
        _GamePhase gamePhase;

        public enum _PlayerPhase { LEFT, RIGHT, NORMAL }
        private static _PlayerPhase playerPhase = _PlayerPhase.NORMAL;

        void Awake()
        {
            instance = GameController.GetInstance();
            _animator = GetComponent<Animator>();
        }

        void Update()
        {
            gamePhase = GameController.GetGamePhase();

            if (gamePhase == _GamePhase.START)
            {
                StartCoroutine("PlayerStart");

                GameController.SetGamePhase(_GamePhase.IDLE);
                return;
            }

            if (gamePhase == _GamePhase.PLAY)
            {
                if (playerPhase == _PlayerPhase.LEFT && Global.direction == Vector3.left)
                    return;
                if (playerPhase == _PlayerPhase.RIGHT && Global.direction == Vector3.right)
                    return;
                playerPhase = _PlayerPhase.NORMAL;
                if(Global.direction == Vector3.right)
                    transform.Translate(Vector3.up * Time.deltaTime * Global.player_speed);
                else
                    transform.Translate(Vector3.down * Time.deltaTime * Global.player_speed);
            }
        }

        IEnumerator PlayerStart()
        {
            transform.parent = instance.transform;

            while(transform.position.y < 7.0f)
            {
                yield return null;
                transform.position += Vector3.right * Time.deltaTime * Global.player_speed;
                transform.position += Vector3.up * Time.deltaTime * Global.map_speed * 0.25f;

                //if (transform.localRotation.x < Global.playerNormalRot.x)
                //{
                //    transform.Rotate(new Vector3(3f, 0, 0));
                //}
                //if (transform.localRotation.y < Global.playerNormalRot.y)
                //{
                //    transform.Rotate(new Vector3(0, 2.5f, 0));
                //}
                //if (transform.localRotation.z < Global.playerNormalRot.z)
                //{
                //    transform.Rotate(new Vector3(0, 0, 4.5f));
                //}

                if (transform.position.z > 50)
                    transform.position -= new Vector3(0, 0, 0.1f);
            }

            transform.localRotation = Quaternion.Euler(Global.playerNormalRot);

            while(transform.position.y > Global.playerNormalPosY)
            {
                yield return null;

                transform.position += (Vector3.right * Time.deltaTime * Global.player_speed);
                transform.position += (Vector3.down * Time.deltaTime * Global.map_speed);
            }
            _animator.Play("Looking_Around_2");
            GameController.SetGamePhase(_GamePhase.PLAY);
        }

        void OnCollisionEnter(Collision collision)
        {
            //foreach(ContactPoint contact in collision.contacts)
            string tag = collision.gameObject.tag;
            if (tag == "Obstacle")
            {
                GameController.SetGamePhase(_GamePhase.OVER);
                Destroy(gameObject);
            }
            else if(tag == "Bonus")
            {
                Destroy(collision.gameObject);
                if(instance != null)
                    instance.CoinUp();
            }
            else if(tag == "Bonus_child")
            {
                Destroy(collision.transform.parent.gameObject);
                if (instance != null)
                    instance.CoinUp();
            }
            else if(tag == "Upgrade")
            {
                Destroy(collision.gameObject);
            }
        }

        void OnCollisionStay(Collision collision)
        {
            string tag = collision.gameObject.tag;
            if(tag == "Wall_left")
            {
                playerPhase = _PlayerPhase.LEFT;
            }
            else if(tag == "Wall_right")
            {
                playerPhase = _PlayerPhase.RIGHT;
            }
        }
    }
}
