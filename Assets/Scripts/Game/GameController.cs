﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using APO;

namespace APO {

    public enum _GamePhase {
        INIT,
        START,
        PLAY,
        OVER,
        IDLE
    }

    public class GameController : MonoBehaviour
    {
        private static _GamePhase gamePhase = _GamePhase.IDLE;
        public static _GamePhase GetGamePhase() { return gamePhase; }
        public static void SetGamePhase(_GamePhase state) { gamePhase = state; }


        private static GameController instance;
        public static GameController SetInstance() { return instance ?? (instance = (GameController)FindObjectOfType(typeof(GameController))); }
        public static GameController GetInstance() { return instance; }
        //public static Transform GetTransform() { return instance != null ? instance.transform : null; }

        
        [SerializeField] private Transform Maps;
        [SerializeField] private TextMesh txt_score;
        [SerializeField] private TextMesh txt_coin;

        [SerializeField] private GameObject Pre_player;
        [SerializeField] private GameObject Pre_car;
        [SerializeField] private GameObject Pre_background;
        [SerializeField] private GameObject Pre_wall;

        private GameObject player;
        private GameObject car;

        private GameObject old_back;
        private GameObject prev_back;
        private GameObject curr_back;
        private GameObject next_back;
        private int next_back_num;

        private GameObject prev_wall;
        private GameObject curr_wall;
        private GameObject next_wall;
        private int next_wall_num;

        private int score;
        private int coin;
        private float distance;

        void Awake() {
            if (instance == null) instance = this;
        }

        IEnumerator Start() {
            yield return null;
            gamePhase = _GamePhase.INIT;
        }

        void Update() {
            switch(gamePhase)
            {
                case _GamePhase.INIT:
                    GameInit();
                    break;
                case _GamePhase.PLAY:
                    Maps.Translate(Vector3.up * Time.deltaTime * Global.map_speed);
                    distance += Time.deltaTime * Global.map_speed / 4;
                    RefreshScore();
                    if (curr_back.transform.position.y >= 2)
                        MakeNewBack();
                    if (curr_wall.transform.position.y >= 0)
                        MakeNewWall();
                    break;
                case _GamePhase.OVER:
                    Time.timeScale = 0;
                    StartCoroutine("Restart");
                    break;
            }
        }

        private void GameInit()
        {
            Maps.transform.localPosition = new Vector3(0, 0, 50);
            foreach (Transform child in Maps)
                Destroy(child.gameObject);

            //player = Instantiate(Pre_player) as GameObject;
            //player.transform.parent = transform;
            //player.transform.localPosition = Global.playerStartPos;
            //player.name = "Player";

            car = Instantiate(Pre_car) as GameObject;
            car.transform.parent = Maps;
            car.transform.localPosition = Global.carPos;
            car.name = "Car";

            player = car.transform.Find("Player").gameObject;

            next_back_num = 0;
            curr_back = Instantiate(Pre_background) as GameObject;
            curr_back.transform.parent = Maps;
            curr_back.transform.localPosition = Global.backStartPos;
            curr_back.name = "Back" + next_back_num;
            next_back_num++;

            next_back = Instantiate(Pre_background) as GameObject;
            next_back.transform.parent = Maps;
            next_back.transform.localPosition = curr_back.transform.localPosition + new Vector3(0, Global.backInterval, 0);
            next_back.name = "Back" + next_back_num;
            next_back_num++;

            next_wall_num = 0;
            curr_wall = Instantiate(Pre_wall) as GameObject;
            curr_wall.transform.parent = Maps;
            curr_wall.transform.localPosition = Global.wallStartPos;
            curr_wall.name = "Wall" + next_wall_num;
            next_wall_num++;

            next_wall = Instantiate(Pre_wall) as GameObject;
            next_wall.transform.parent = Maps;
            next_wall.transform.localPosition = curr_wall.transform.localPosition + new Vector3(0, Global.wallInterval, 0);
            next_wall.name = "Wall" + next_wall_num;
            next_wall_num++;

            Global.direction = Vector3.right;

            score = 0;
            coin = 0;
            distance = 0.0f;
            txt_score.text = score.ToString();
            txt_coin.text = coin.ToString();
            gamePhase = _GamePhase.START;
        }

        private void MakeNewBack()
        {
            if (old_back != null) Destroy(old_back);
            old_back = prev_back;
            prev_back = curr_back;
            curr_back = next_back;

            next_back = Instantiate(Pre_background) as GameObject;
            next_back.transform.parent = Maps;
            next_back.transform.localPosition = curr_back.transform.localPosition + new Vector3(0, Global.backInterval, 0);
            next_back.name = "Back" + next_back_num;
            next_back_num++;
        }

        private void MakeNewWall()
        {
            if (prev_wall != null) Destroy(prev_wall);
            prev_wall = curr_wall;
            curr_wall = next_wall;

            next_wall = Instantiate(Pre_wall) as GameObject;
            next_wall.transform.parent = Maps;
            next_wall.transform.localPosition = curr_wall.transform.localPosition + new Vector3(0, Global.wallInterval, 0);
            next_wall.name = "Wall" + next_wall_num;
            next_wall_num++;
        }

        public void RefreshScore()
        {
            score = (int)distance;
            txt_score.text = score.ToString();
        }

        public void CoinUp()
        {
            coin++;
            txt_coin.text = coin.ToString();
        }

        IEnumerator Restart()
        {
            gamePhase = _GamePhase.IDLE;
            Time.timeScale = 1;
            yield return new WaitForSeconds(2.0f);
            gamePhase = _GamePhase.INIT;
        }

        void OnFingerDown(FingerDownEvent e)
        {
            if (e.Selection == null)
                return;

            if(e.Selection.tag == "TouchPan")
            {
                Global.direction = Global.direction == Vector3.right ? Vector3.left : Vector3.right;
                //if (player != null)
                //    player.transform.localRotation = Quaternion.Euler(Global.direction == Vector3.right ? new Vector3(20, 0, 0) : new Vector3(-20, 180, 0));
            }
        }
    }
}
