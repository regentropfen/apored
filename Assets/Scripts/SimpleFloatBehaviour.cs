﻿using UnityEngine;


public class SimpleFloatBehaviour : MonoBehaviour {
    
    public float amplitude = 2f;
    //Set in Inspector
    public float speed = 1f;
    //Set in Inspector
    private float tempVal;

    private Vector3 tempPos;

    public bool local = false;

    void Start() {
        if (local) {
            tempPos = transform.localPosition;
            tempVal = transform.localPosition.y;
        } else {
            tempPos = transform.position;
            tempVal = transform.position.y;
        }
    }

    void Update() {        
        tempPos.y = tempVal + amplitude * Mathf.Sin(speed * Time.time);
        if (local) {
            transform.localPosition = tempPos;
        } else {
            transform.position = tempPos;
        }
    }
}