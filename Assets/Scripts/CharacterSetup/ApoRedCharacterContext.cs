﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApoRedCharacterContext {

	private static ApoRedCharacterContext singleton;
	private static bool initialized = false;

	public List<CharacterTypeProperties> CharacterList = new List<CharacterTypeProperties>
	{
		new CharacterTypeProperties(), 
		new CharacterTypeProperties(CharacterType.DIVERED, "DiveRed",false,1000),
		new CharacterTypeProperties(CharacterType.SPACERED, "SpaceRed",false,1000)
	};
    public enum CharacterType
    {
        STANDARDRED,
        SPACERED,
        DIVERED
    }

	public static ApoRedCharacterContext Singleton
    {
        get
        {
            if (!initialized)
            {
                initialized = true;
				singleton = new ApoRedCharacterContext();
            }
            return singleton;
        }
    }

	public CharacterType CurrentCharacter;
	public ApoRedCharacterContext()
    {

    }
}

public class CharacterTypeProperties{
	public	ApoRedCharacterContext.CharacterType Type = ApoRedCharacterContext.CharacterType.STANDARDRED;
	public string Name = "RealRed";
	public bool Purchased = true;
	public int Price = 1000;

	public CharacterTypeProperties(){
		
	}

	public void SetProperties(ApoRedCharacterContext.CharacterType type, string name, bool purchased, int price){

        Type = type;
        Name = name;
        Purchased = purchased;
        Price = price;
	}

	public CharacterTypeProperties( ApoRedCharacterContext.CharacterType type, string name, bool purchased, int price)
    {
		SetProperties( type,  name,  purchased,  price);
    }
}
