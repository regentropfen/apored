﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCloudBehaviour : MonoBehaviour {

	Vector3 StartPosition;
	public float MinSpeed = 1;
	public float MaxSpeed = 2;
	public float Speed;

	// Use this for initialization
	void Start () {
		StartPosition = transform.position;
		Speed = Random.Range(MinSpeed, MaxSpeed)*0.005f;
		gameObject.name = "ToonCloud";
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.Translate(Vector3.down * Speed);
	}

	void Respawn(){
		GameObject cloud = Instantiate(gameObject);
		cloud.transform.position = StartPosition;
		Destroy(gameObject);
	}

	private void OnTriggerEnter(Collider col)
	{	
		if(col.gameObject.name == "CloudCollider"){
			Respawn();
		}
	}

}
