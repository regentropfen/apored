﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideOnTimeBehaviour : MonoBehaviour {

public float TimeUntilHiding = 5f;
	// Use this for initialization
	void Start () {
		HideAfterSeconds(TimeUntilHiding);
	}
	void HideAfterSeconds(float timeToWait){
		Invoke("Hide", timeToWait);
	}

	void Hide(){
		gameObject.SetActive(false);
	}
}
