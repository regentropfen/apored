﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApoRedCharacterSelectionControllerBehaviour : MonoBehaviour {

    #region ApoRedParts

	public GameObject ApoRed;
	//attachedParts (needs to bechanged if model changes)
	GameObject standardBelt;
	GameObject Body;
	GameObject HeadObject;
	GameObject HeadBone;
	GameObject LeftHandBone;
	GameObject RightHandBone;

    #endregion

	ApoRedCharacterContext CharacterContext;
	MainGameContext GameContext;
	ApoRedCharacterContext.CharacterType LastCharacter;
    //If you want to use it only to set up the character once
	public bool DestroyAfterInitializing = false;

	#region StandardRed
    
	public Material StandardRedMaterial;
    public GameObject StandardRedHeadAccessoire;
	public RuntimeAnimatorController StandardRedMenuAnimator;
	public RuntimeAnimatorController StandardRedGameAnimator;
	public GameObject StandardRedLeftHandTool;
	public GameObject StandardRedRightHandTool;

    #endregion
	#region SpaceRed

	public Material SpaceRedMaterial;
	public GameObject SpaceRedHeadAccessoire;
	public RuntimeAnimatorController SpaceRedMenuAnimator;
	public RuntimeAnimatorController SpaceRedGameAnimator;
	public GameObject SpaceRedLeftHandTool;
	public GameObject SpaceRedRightHandTool;

    #endregion
	
	void Start () {
		GameContext = MainGameContext.Singleton;
		CharacterContext = ApoRedCharacterContext.Singleton;
		FindCharacterElements();
		SetCharacter(CharacterContext.CurrentCharacter);
		if(DestroyAfterInitializing){
			Destroy(this);
		}
	}

    void FindCharacterElements()
	{
		if (ApoRed == null)
        {
			ApoRed= GameObject.FindWithTag("MainCharacter");
			if(ApoRed == null){
				print("ApoRedCharacter not found - exiting ...");
				Destroy(gameObject);
			}
        }
	}	
	
	void Update () {
		if (LastCharacter != CharacterContext.CurrentCharacter){
			SetCharacter(CharacterContext.CurrentCharacter);
		}
	}

	void SetStandardCharacter(){
		SetCharacter(StandardRedMaterial, StandardRedHeadAccessoire, StandardRedMenuAnimator, StandardRedGameAnimator, StandardRedLeftHandTool, StandardRedRightHandTool);
	}

	void SetCharacter(ApoRedCharacterContext.CharacterType characterType){
		SetStandardCharacter();
		if(characterType == ApoRedCharacterContext.CharacterType.SPACERED){
			SetCharacter(SpaceRedMaterial, SpaceRedHeadAccessoire, SpaceRedMenuAnimator, SpaceRedGameAnimator, SpaceRedLeftHandTool, SpaceRedRightHandTool);
		}
		LastCharacter = CharacterContext.CurrentCharacter;
	}

	void SetCharacter(Material material, GameObject headAccessoire, RuntimeAnimatorController menuAnimator, RuntimeAnimatorController gameAnimator,GameObject leftHandTool,GameObject rightHandTool){
		if(material!=null){
			standardBelt.GetComponent<Renderer>().material = material;
			Body.GetComponent<Renderer>().material = material;
			HeadObject.GetComponent<Renderer>().material = material;
		}
		if(headAccessoire !=null){
			headAccessoire.transform.SetParent(HeadBone.transform);
		}
		if (leftHandTool != null)
        {
			leftHandTool.transform.SetParent(LeftHandBone.transform);
        }
		if (rightHandTool != null)
        {
			rightHandTool.transform.SetParent(RightHandBone.transform);
        }
		if (leftHandTool != null)
        {
            leftHandTool.transform.SetParent(LeftHandBone.transform);
        }
		RuntimeAnimatorController redAnimator = ApoRed.GetComponentInChildren<RuntimeAnimatorController>();
		if (menuAnimator != null)
        {
			redAnimator = menuAnimator;
        }
		if(GameContext.CurrentGameState == MainGameContext.GameStates.INGAME){
			if (gameAnimator != null)
            {
				redAnimator = gameAnimator;
            }	
		}
	}
}
