﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ObjectSizeOutput : MonoBehaviour {
   
    public float length;

    public float height;

    public float width;

    // Use this for initialization
    void Start() {
        
    }
	
    // Update is called once per frame
    void Update() {
        Vector3 extends = gameObject.GetComponent<MeshRenderer>().bounds.extents;
        width = extends.z;
        height = extends.y;
        length = extends.x;
    }
}
