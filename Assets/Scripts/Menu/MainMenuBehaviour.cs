﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuBehaviour : MonoBehaviour {
	MainGameContext GameContext;
	public enum MenuOptions{
		OPEN_MAIN_MENU,
        OPEN_PREFERENCES_MENU,
        OPEN_CHARACTER_SELECTION_MENU,
        OPEN_GAME_MODE_MENU,
        OPEN_HELP_MENU,
        OPEN_LEVEL_WON_MENU,
        OPEN_LEVEL_LOST_MENU,
        COUNTINUE_GAME,
        NEXT_LEVEL,
        RESTART_GAME,
        GET_EXTRA_MONEY,
        PLAY_AD,
	}

	public enum MenuState{
		MAIN_MENU,
        LEVEL_WON,
        LEVEL_LOST
	}
	public MenuState CurrentMenuState = MenuState.MAIN_MENU;

	//Menu rotation Speed
	const float Speed = 7f;

	public GameObject MainMenu;
	public GameObject PreferencesMenu;
	public GameObject LevelLostMenu;
	public GameObject GameModeMenu;
	public GameObject CharacterSelectionMenu;
	public GameObject LevelWonMenu;
	public GameObject CountinueLevelButton;
	public GameObject ExtraCoinsButton;
	public GameObject CharacterSelectionMenuPrefab;

	public GameObject CurrentMenu;

	public List<GameObject> AllMenus = new List<GameObject>();
	// Use this for initialization
	void Start () {
		GameContext = MainGameContext.Singleton;
		GameContext.CurrentGameState = MainGameContext.GameStates.MENU;
		DeactivateAllMenus();
		if (CurrentMenuState == MenuState.LEVEL_WON)
		{
			HitMenuItem(MenuOptions.OPEN_LEVEL_WON_MENU);
		}else if (CurrentMenuState == MenuState.LEVEL_LOST)
        {
			HitMenuItem(MenuOptions.OPEN_LEVEL_LOST_MENU);
		//}else if (CurrentMenuState == MenuState.MAIN_MENU)
        //{
            //HitMenuItem(MenuOptions.OPEN_MAIN_MENU);
		}else
        {
			HitMenuItem(MenuOptions.OPEN_LEVEL_LOST_MENU);
        }
	}

	void DeactivateAllMenus(){
		foreach(GameObject menu in AllMenus){
			menu.transform.rotation = Quaternion.Euler(Vector3.up*90);
			menu.SetActive(false);
		}
		DeactivateBoni();
	}

	void DeactivateBoni(){
		CountinueLevelButton.SetActive(false);
        ExtraCoinsButton.SetActive(false);
	}


	public void HitMenuItem(MenuOptions menuOption){
		if(menuOption == MenuOptions.COUNTINUE_GAME){
			CountinueGame();	
		}else if(menuOption == MenuOptions.OPEN_MAIN_MENU){
			LoadMainMenu();
		}else if (menuOption == MenuOptions.OPEN_GAME_MODE_MENU)
        {
			LoadGameModeMenu();
		}else if (menuOption == MenuOptions.OPEN_LEVEL_WON_MENU)
        {
			LoadLevelWonMenu();
		}else if (menuOption == MenuOptions.OPEN_LEVEL_LOST_MENU)
        {
			LoadLevelLostMenu();
		}else if (menuOption == MenuOptions.OPEN_PREFERENCES_MENU)
        {
            LoadPrefMenu();
		}else if (menuOption == MenuOptions.OPEN_HELP_MENU)
        {
			OpenHelpMenu();
        }else if (menuOption == MenuOptions.OPEN_CHARACTER_SELECTION_MENU)
        {
			LoadCharSelectMenu();
		}else if (menuOption == MenuOptions.GET_EXTRA_MONEY)
        {
            LoadMainMenu();
		}else if (menuOption == MenuOptions.RESTART_GAME)
        {
            LoadMainMenu();
		}else if (menuOption == MenuOptions.PLAY_AD)
        {
            LoadMainMenu();
        }
	}

	void CountinueGame(){
		SceneManager.LoadScene("Game");
	}

	void LoadMainMenu(){
		StartCoroutine(MoveInMenu(MainMenu.transform));
	}
	void LoadPrefMenu()
    {
		StartCoroutine(MoveInMenu(PreferencesMenu.transform));
    }
	void LoadCharSelectMenu()
    {
		print("CharacterSelectionMenu geladen");
		StartCoroutine(MoveOutMenu(CurrentMenu.transform)); 
		CharacterSelectionMenu= Instantiate(CharacterSelectionMenuPrefab);
		CharacterSelectionMenu.GetComponent<CharacterSelectionCanvasBehaviour>().MainMenu = this;
		CurrentMenu = CharacterSelectionMenu;
		//StartCoroutine(MoveInMenu(CharacterSelectionMenu.transform));
    }

	void LoadGameModeMenu()
    {
		print("GameModeMenu geladen");
		//StartCoroutine(MoveInMenu(GameModeMenu.transform));
    }

	void LoadLevelWonMenu()
    {
		StartCoroutine(MoveInMenu(LevelWonMenu.transform));
		ExtraCoinsButton.SetActive(true);
    }

	void LoadLevelLostMenu()
    {
		StartCoroutine(MoveInMenu(LevelLostMenu.transform));
		CountinueLevelButton.SetActive(true);
		ExtraCoinsButton.SetActive(true);
    }

	void GetExtraMoney(){
		print("Get extra Money");
		PlayAd();
	}

	void PlayAd(){
		print("PlayingAd");
	}

	void RestartGame(){
		SceneManager.LoadScene("Game");
	}

	void OpenHelpMenu(){
		print("Opening Help Menu");
	}


	IEnumerator MoveOutMenu(Transform Menu){
		DeactivateBoni();
		Quaternion targetRotation = Quaternion.Euler(Vector3.up* 270f);
		float waitTime = Time.time + 1f;
		while (!Quaternion.Equals(Menu.rotation, targetRotation)&&waitTime>Time.time)
		{
			Menu.rotation = Quaternion.Lerp(Menu.rotation, targetRotation, Time.deltaTime * Speed);
			print("Coruitine running: Move out");
			yield return null;
		}
		Menu.rotation = targetRotation;
		Menu.gameObject.SetActive(false);
	}

	IEnumerator MoveInMenu(Transform Menu)
    {
		if (CurrentMenu != null)
		{
			//Check if it is inherited from abstract menu behaviour
			UI_AbstractMenuBehaviour menuBehaviour = CurrentMenu.GetComponent<UI_AbstractMenuBehaviour>();
			if (menuBehaviour!=null){
				menuBehaviour.CloseMenu();
			}else{            
                StartCoroutine(MoveOutMenu(CurrentMenu.transform));	
			}
		}
		Menu.gameObject.SetActive(true);
		Quaternion targetRotation =Quaternion.Euler( Vector3.zero);
		float waitTime = Time.time + 1f;
        while (!Quaternion.Equals(Menu.rotation, targetRotation) && waitTime > Time.time)
        {
            Menu.rotation = Quaternion.Lerp(Menu.rotation, targetRotation, Time.deltaTime * Speed);
			print("Coruitine running: Move in");
            yield return null;
        }
        Menu.rotation = targetRotation;
		CurrentMenu = Menu.gameObject;
        
    }
}
