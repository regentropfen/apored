﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameContext {
	private static MainGameContext singleton;

	public enum GameType{
		STANDARD,
        SKYSCRAPER,
        SPACE
	}
 

	public enum GameStates{
		MENU,
        INGAME,
	}

    private static bool initialized = false;

    public static MainGameContext Singleton
    {
        get
        {
            if (!initialized)
            {
                initialized = true;
                singleton = new MainGameContext();
            }
            return singleton;
        }
    }

    public MainGameContext()
    {

    }

	public GameStates CurrentGameState = GameStates.MENU;
	public Dictionary<GameType, int> FallenMeters;
	public int RedCoins;

	public bool Sound = true;


    /// <summary>
    /// Loads last properties and achievements
    /// </summary>
    void Load()
    {

    }
}
