﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestorePurchasesButtonBehaviour : AbstractMenuItemBehaviour {
	public GameObject RestorePurchasesMenuPrefab;

	public override void Hit()
	{
		Instantiate(RestorePurchasesMenuPrefab);
	}
}
