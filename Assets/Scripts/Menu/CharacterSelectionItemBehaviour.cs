﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectionItemBehaviour : MonoBehaviour
{
	public CharacterSelectionCanvasBehaviour SelectionCanvasBehaviour;
	public CharacterTypeProperties CharacterProperties;
	public Sprite itemSprite;
	public Image ItemImage;
	public Text ItemName;
	public Text ItemCosts;
	public GameObject PurchaseButton;
	ApoRedCharacterContext CharacterContext;


	// Use this for initialization
	void Start()
	{
		CharacterContext = ApoRedCharacterContext.Singleton;
		ItemImage.sprite = itemSprite;
		ItemName.text = CharacterProperties.Name;


		if (CharacterProperties.Purchased)
		{
			Destroy(PurchaseButton);
		}
		else
		{
			ItemCosts.text = CharacterProperties.Price.ToString();

		}
	}


    /// <summary>
    /// Marks the Button as selected
    /// </summary>
    /// <param name="active">If set to <c>true</c> active.</param>
	public void Activate(bool active = true){
		//TODO
	}
    
	public void PurchaseCharacter(){
		MainGameContext gameContext = MainGameContext.Singleton;
		if(gameContext.RedCoins >CharacterProperties.Price){
			gameContext.RedCoins -= CharacterProperties.Price;
			CharacterProperties.Purchased = true;
			SelectCharacter();
		}else{
			SelectionCanvasBehaviour.OpenInAppPurchase();
		}
	}

	public void SelectCharacter(){
		SelectionCanvasBehaviour.SelectCharacter(CharacterProperties);
	}

}
