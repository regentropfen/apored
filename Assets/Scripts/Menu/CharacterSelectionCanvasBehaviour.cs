﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectionCanvasBehaviour : UI_AbstractMenuBehaviour {

    //Neds an active AppearenceController in the Scene to work!!
	public GameObject CharacterSelectionItemPanelPrefab;
	public RectTransform ScrollViewContentPanel;
	public GameObject PurchaseRedCoinsPrefab;
	public MainMenuBehaviour MainMenu;

	List<CharacterSelectionItemBehaviour> CharacterSelectionItemBehaviours = new List<CharacterSelectionItemBehaviour>();
	//Images must be in the order of Items in the Achievement enum!
	public List<Sprite> AchievementSpritesList;

	ApoRedCharacterContext CharacterContext;
	//ApoRedCharacterSelectionControllerBehaviour SelectionController;
	// Use this for initialization
	void Start () {
		CharacterContext = ApoRedCharacterContext.Singleton;
		BuildCharacterSelectionMenu();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void BuildCharacterSelectionMenu()
    {
		
		for (int i = 0; i < CharacterContext.CharacterList.Count;i++)
        {
			GameObject button = Instantiate(CharacterSelectionItemPanelPrefab);
			button.transform.SetParent(ScrollViewContentPanel, false);
			CharacterSelectionItemBehaviour itemBehaviour = button.GetComponent<CharacterSelectionItemBehaviour>();
			CharacterSelectionItemBehaviours.Add(itemBehaviour);
			if (AchievementSpritesList.Count <= CharacterContext.CharacterList.Count)
			{
				itemBehaviour.itemSprite = AchievementSpritesList[i];
			}
			itemBehaviour.CharacterProperties = CharacterContext.CharacterList[i];

			itemBehaviour.SelectionCanvasBehaviour = this;

        }
    }

	public void DeactivateAllSelectionBehaviours(){
		//TODO
		foreach(CharacterSelectionItemBehaviour item in CharacterSelectionItemBehaviours){
			item.Activate(false);
		}
	}

	public void OpenInAppPurchase(){
		Instantiate(PurchaseRedCoinsPrefab);
	}

	public void SelectCharacter(CharacterTypeProperties characterType){
		CharacterContext.CurrentCharacter = characterType.Type;
	}

	public void ExitMenu(){
		MainMenu.HitMenuItem(MainMenuBehaviour.MenuOptions.OPEN_MAIN_MENU);
	}
}
