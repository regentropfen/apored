﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractMenuItemBehaviour : MonoBehaviour {

	protected MainGameContext GameContext;

	public MainMenuBehaviour mainMenuBehaviour;

	public MainMenuBehaviour.MenuOptions MenuOption = MainMenuBehaviour.MenuOptions.OPEN_MAIN_MENU;

	protected virtual void Start(){
		GameContext = MainGameContext.Singleton;
	}


	public virtual void Hit(){
		mainMenuBehaviour.HitMenuItem(MenuOption);
	}
}
