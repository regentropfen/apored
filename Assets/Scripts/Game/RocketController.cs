﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using APO;

namespace APO
{
    public class RocketController : MonoBehaviour
    {
        void Update()
        {
            transform.Translate(Vector3.up * Time.deltaTime * Global.rocket_speed);
        }
    }
}
