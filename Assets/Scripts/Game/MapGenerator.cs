﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using APO;

namespace APO
{
    public class MapGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject Pre_obst_cloud_small;
        [SerializeField] private GameObject Pre_obst_cloud_medium;
        [SerializeField] private GameObject Pre_obst_cloud_large_left;
        [SerializeField] private GameObject Pre_obst_cloud_large_right;
        [SerializeField] private GameObject Pre_obst_cloud_large_both;
        [SerializeField] private GameObject Pre_obst_rocket1;
        [SerializeField] private GameObject Pre_obst_rocket2_1;
        [SerializeField] private GameObject Pre_obst_rocket2_2;
        [SerializeField] private GameObject Pre_obst_rocket2_3;
        [SerializeField] private GameObject Pre_obst_rocket2_horiz;
        [SerializeField] private GameObject Pre_obst_rocket3_1;
        [SerializeField] private GameObject Pre_obst_rocket3_2;
        [SerializeField] private GameObject Pre_obst_rocket3_3;
        [SerializeField] private GameObject Pre_obst_rocket3_4;
        [SerializeField] private GameObject Pre_obst_rocket3_diag_1;
        [SerializeField] private GameObject Pre_obst_rocket3_diag_2;
        [SerializeField] private GameObject Pre_obst_static_saucer;
        [SerializeField] private GameObject Pre_obst_moving_saucer;

        [SerializeField] private GameObject Pre_bonus_pistol;
        [SerializeField] private GameObject Pre_bonus_barreled;
        [SerializeField] private GameObject Pre_bonus_bazooka;
        [SerializeField] private GameObject Pre_bonus_set;

        [SerializeField] private List<GameObject> Pre_upgrade_parachutes;
        [SerializeField] private GameObject Pre_upgrade_armor;
        [SerializeField] private GameObject Pre_upgrade_bag;

        private static _GamePhase gamePhase = _GamePhase.IDLE;

        private GameObject madeObject;
        private float next_div;

        void Update()
        {
            gamePhase = GameController.GetGamePhase();
            if (gamePhase == _GamePhase.START && madeObject == null)
                Init();
            if (gamePhase == _GamePhase.PLAY)
            {
                GeneratingMaps();
            }
            else if(gamePhase == _GamePhase.OVER)
            {
                StopCoroutine("GenerateRockets");
            }
        }

        private void GeneratingMaps()
        {
            if (madeObject.transform.position.y > Global.generatingPosY + next_div)
            {
                float rnd = Random.value;
                if (rnd < 0.1f)
                {
                    MakeCloudSmall();
                }
                else if (rnd < 0.2f)
                {
                    MakeCloudMedium();
                }
                else if (rnd < 0.3f)
                {
                    MakeCloudLargeLeft();
                }
                else if (rnd < 0.4f)
                {
                    MakeCloudLargeRight();
                }
                else if (rnd < 0.5f)
                {
                    MakeBonusSet();
                }
                else if (rnd < 0.6f)
                {
                    MakeArmor();
                }
                else if (rnd < 0.7f)
                {
                    MakeBag();
                }
                else if (rnd < 0.8f)
                {
                    MakePistol();
                }
                else if (rnd < 0.9f)
                {
                    MakeBarreled();
                }
                else
                {
                    MakeBazooka();
                }

                next_div = Global.minDivY + Random.Range(1.0f, 5.0f);
            }
        }

        void Init()
        {
            madeObject = Instantiate(Pre_bonus_set) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(0, -8, 50);
            madeObject.name = "bonusSet";

            madeObject = Instantiate(Pre_obst_cloud_large_both) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(0, Global.generatingPosY, 50);
            madeObject.name = "cloudLargeBoth";

            next_div = Global.minDivY + Random.Range(1.0f, 10.0f);
            StartCoroutine("GenerateRockets");
        }

        IEnumerator GenerateRockets()
        {
            float waitTime = Random.Range(5.0f, 10.0f);
            while (gamePhase == _GamePhase.START || gamePhase == _GamePhase.PLAY)
            {
                yield return new WaitForSeconds(waitTime);
                float rnd = Random.value;
                if (rnd < 0.2f)
                {
                    MakeRocket1();
                }
                else if (rnd < 0.4f)
                {
                    MakeRocket2_1();
                }
                else if (rnd < 0.6f)
                {
                    MakeRocket2_2();
                }
                else if (rnd < 0.8f)
                {
                    MakeRocket2_3();
                }
                else
                {
                    MakeRocket3_1();
                }

                waitTime = Random.Range(5.0f, 15.0f);
            }
        }

        void MakeCloudSmall()
        {
            madeObject = Instantiate(Pre_obst_cloud_small) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-3.5f, 3.5f), Global.generatingPosY, 50);
            madeObject.name = "cloudSmall";
        }

        void MakeCloudMedium()
        {
            madeObject = Instantiate(Pre_obst_cloud_medium) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-3f, 3f), Global.generatingPosY, 50);
            madeObject.name = "cloudMedium";
        }

        void MakeCloudLargeLeft()
        {
            madeObject = Instantiate(Pre_obst_cloud_large_left) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(-4.5f, Global.generatingPosY, 50);
            madeObject.name = "cloudLargeLeft";
        }

        void MakeCloudLargeRight()
        {
            madeObject = Instantiate(Pre_obst_cloud_large_right) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(4.5f, Global.generatingPosY, 50);
            madeObject.name = "cloudLargeRight";
        }

        void MakeBonusSet()
        {
            madeObject = Instantiate(Pre_bonus_set) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(0, Global.generatingPosY, 50);
            madeObject.name = "BonusSet";
        }

        void MakeArmor()
        {
            madeObject = Instantiate(Pre_upgrade_armor) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-4.5f, 4.5f), Global.generatingPosY, 50);
            madeObject.name = "Armor";
        }

        void MakeBag()
        {
            madeObject = Instantiate(Pre_upgrade_bag) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-4.5f, 4.5f), Global.generatingPosY, 50);
            madeObject.name = "Bag";
        }

        void MakePistol()
        {
            madeObject = Instantiate(Pre_bonus_pistol) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-4.5f, 4.5f), Global.generatingPosY, 50);
            madeObject.name = "Pistol";
        }

        void MakeBarreled()
        {
            madeObject = Instantiate(Pre_bonus_barreled) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-4f, 4f), Global.generatingPosY, 50);
            madeObject.name = "Barreled";
        }

        void MakeBazooka()
        {
            madeObject = Instantiate(Pre_bonus_bazooka) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-3.5f, 3.5f), Global.generatingPosY, 50);
            madeObject.name = "Bazooka";
        }

        void MakeRocket1()
        {
            madeObject = Instantiate(Pre_obst_rocket1) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-4.5f, 4.5f), Global.generatingPosY, 50);
            madeObject.name = "Rocket1";
        }

        void MakeRocket2_1()
        {
            madeObject = Instantiate(Pre_obst_rocket2_1) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-3f, 3f), Global.generatingPosY, 50);
            madeObject.name = "Rocket2_1";
        }

        void MakeRocket2_2()
        {
            madeObject = Instantiate(Pre_obst_rocket2_2) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-3f, 3f), Global.generatingPosY, 50);
            madeObject.name = "Rocket2_2";
        }

        void MakeRocket2_3()
        {
            madeObject = Instantiate(Pre_obst_rocket2_3) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(0, Global.generatingPosY, 50);
            madeObject.name = "Rocket2_3";
        }

        void MakeRocket3_1()
        {
            madeObject = Instantiate(Pre_obst_rocket3_1) as GameObject;
            madeObject.transform.parent = transform;
            madeObject.transform.position = new Vector3(Random.Range(-2.5f, 2.5f), Global.generatingPosY, 50);
            madeObject.name = "Rocket3_1";
        }
    }
}