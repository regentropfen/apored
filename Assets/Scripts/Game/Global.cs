﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour {
    public static Vector3 playerStartPos = new Vector3(-0.87f, 6.7f, 50);
    public static Vector3 playerNormalRot = new Vector3(60, 180, 90);
    public static float playerNormalPosY = 2.5f;

    public static Vector3 carPos = new Vector3(-2.64f, 3.71f, 4.5f);
    public static Vector3 backStartPos = new Vector3(0, 2, 20);
    public static float backInterval = -17;
    public static Vector3 wallStartPos = Vector3.zero;
    public static float wallInterval = -20;

    public static float map_speed = 7.0f;
    public static float rocket_speed = 3.0f;
    public static float player_speed = 8.0f;
    public static Vector3 direction = Vector3.right;

    public static float generatingPosY = -14.0f;
    public static float minDivY = 5.0f;
}
